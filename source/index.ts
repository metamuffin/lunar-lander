/// <reference lib="dom" />

import { Collider } from "./collider.ts";
import { get_texture, texture_names } from "./resources.ts";
import { Rocket } from "./rocket.ts";

export const USE_TEXTURES = true

const canvas = document.createElement("canvas")
canvas.style.position = "absolute"
canvas.style.top = "0px"
canvas.style.left = "0px"
canvas.style.width = "100vw"
canvas.style.height = "100vh"
document.body.append(canvas)


const ctx_ = canvas.getContext("2d")
if (!ctx_) throw new Error("sdcaxafjhsdf");
const ctx = ctx_

const r: Rocket = new Rocket()

export type GameState = "lose" | "win" | "ingame"
let game_state: GameState = "ingame"
export const set_game_state = (s: GameState) => game_state = s

const colliders = [
    new Collider(0, -10, 15),
    new Collider(20, -10, 15),
    new Collider(-20, -10, 15),
    new Collider(50, -5, 20),
    new Collider(-50, -5, 20),
    new Collider(70, 15, 20),
    new Collider(-70, 15, 20),
    new Collider(95, 30, 40),
    new Collider(-95, 30, 40),
]
colliders.sort((a, b) => b.r - a.r)


function resize() {
    const w = canvas.getBoundingClientRect().width
    const h = canvas.getBoundingClientRect().height
    if (canvas.width != w) canvas.width = w
    if (canvas.height != h) canvas.height = h
}

let last_t = Date.now()
const start_time = Date.now()

export const keys_down = new Set()
document.body.addEventListener("keydown", ev => keys_down.add(ev.code))
document.body.addEventListener("keyup", ev => keys_down.delete(ev.code))
export const keys_toggle = new Set()
document.body.addEventListener("keydown", ev => keys_toggle.has(ev.code) ? keys_toggle.delete(ev.code) : keys_toggle.add(ev.code))

let zoom = 0

function redraw() {
    resize()

    const now = game_state == "ingame" ? Date.now() : last_t
    const delta_t = (now - last_t) / 1000
    last_t = now


    if (USE_TEXTURES) {
        const s = Math.max(canvas.height, canvas.width)
        ctx.drawImage(get_texture(texture_names.background), 0, 0, s * 16 / 9, s)
        ctx.fillStyle = "#00000040"
        ctx.fillRect(0, 0, canvas.width, canvas.height)
    } else {
        ctx.fillStyle = "black"
        ctx.fillRect(0, 0, canvas.width, canvas.height)
    }



    ctx.resetTransform()
    ctx.imageSmoothingEnabled = false
    ctx.save()

    const speed = Math.sqrt(r.vx * r.vx + r.vy * r.vy)
    const target_zoom = Math.min(100, Math.max(20, speed * 10))
    zoom += (target_zoom - zoom) * Math.pow(0.00001, delta_t)
    ctx.translate(canvas.width / 2, canvas.height / 2)
    const scale = Math.min(canvas.width, canvas.height) / zoom
    ctx.scale(scale, -scale)
    ctx.translate(-r.x, -r.y)


    if (Math.sqrt(r.x * r.x + r.y * r.y) > 1000) game_state = "lose"

    if (game_state != "lose") r.draw(ctx)
    colliders.forEach(c => c.draw(ctx))

    if (game_state == "ingame") {
        r.tick(delta_t)
        for (const c of colliders) {
            c.tick(r)
        }
    }

    ctx.restore()

    // HUD

    r?.drawHUD(ctx)

    ctx.fillStyle = "black"
    ctx.font = "10px sans-serif"
    ctx.fillText(`Time: ${(now - start_time) / 1000}`, 10, canvas.height - 10)

    ctx.fillStyle = "red"
    ctx.font = "100px sans-serif"
    if (game_state == "ingame" && r.fuel < 0.2 && Math.floor(last_t / 200) % 2 == 0) {
        if (USE_TEXTURES) {
            ctx.drawImage(get_texture(texture_names.text.low_fuel), 20, 100, 260, 130)
        } else {
            ctx.fillText("LOW FUEL", 100, 200)
        }
    }

    if (game_state != "ingame") {
        ctx.fillStyle = "#00000080"
        ctx.fillRect(0, 0, canvas.width, canvas.height)
    }

    ctx.font = "100px sans-serif"
    ctx.textAlign = "center"
    ctx.textBaseline = "middle"
    if (USE_TEXTURES) {
        const s = Math.min(canvas.width / 5, canvas.height / 5)
        const sy = s * 0.25
        if (game_state == "win") {
            ctx.drawImage(get_texture(texture_names.text.landed), canvas.width / 2 - s, canvas.height / 2 - sy, s * 2, sy * 2)
        }
        if (game_state == "lose") {
            ctx.drawImage(get_texture(texture_names.text.boom), canvas.width / 2 - s, canvas.height / 2 - sy, s * 2, sy * 2)
        }
    } else {
        if (game_state == "win") {
            ctx.fillStyle = "#00ff00"
            ctx.fillText("Landed!", canvas.width / 2, canvas.height / 2)
        }
        if (game_state == "lose") {
            ctx.fillStyle = "#ff0000"
            ctx.fillText("BOOM!!!", canvas.width / 2, canvas.height / 2)
        }
    }
    ctx.textAlign = "left"
    ctx.textBaseline = "bottom"

    requestAnimationFrame(redraw)
}
redraw()



