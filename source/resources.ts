const mirror = "https://s.metamuffin.org/media/lunar-lander-artwork/render/"

const texture_el_cache = new Map<string, HTMLImageElement>()

export const texture_names = {
    fuel_bar: [
        "fuel0.kra.png",
        "fuel5.kra.png",
        "fuel10.kra.png",
        "fuel15.kra.png",
        "fuel20.kra.png",
        "fuel25.kra.png",
        "fuel30.kra.png",
        "fuel35.kra.png",
        "fuel40.kra.png",
        "fuel45.kra.png",
        "fuel50.kra.png",
        "fuel55.kra.png",
        "fuel60.kra.png",
        "fuel65.kra.png",
        "fuel70.kra.png",
        "fuel75.kra.png",
        "fuel80.kra.png",
        "fuel85.kra.png",
        "fuel90.kra.png",
        "fuel95.kra.png",
        "fuel100.kra.png",
    ],
    rocket: "rocket.kra.png",
    background: "background.kra.png",
    rocks: [
        "stein15.kra.png",
        "stein37.kra.png",
        "stein40.kra.png",
        "stein58.kra.png",
        "stein64.kra.png",
    ],
    text: {
        boom: "text_boom.kra.png",
        landed: "text_landed.kra.png",
        low_fuel: "Fuel_low.kra.png"
    },
    flame: [
        "flammev2f1.kra.png",
        "flammev2f2.kra.png",
        "flammev2f3.kra.png",
        "flammev2f4.kra.png"
    ],
}

export function get_texture(name: string): HTMLImageElement {
    const url = mirror + name
    const cache = texture_el_cache.get(url)
    if (cache) {
        return cache
    } else {
        const s = document.createElement("img")
        s.src = url
        return s
    }
}

export function get_animated_texture(names: string[], fps: number): HTMLImageElement {
    const i = Math.floor(Date.now() / 1000 * fps) % names.length
    return get_texture(names[i])
}

export function get_fuel_texture(s: number) {
    const n = Math.round((s * 100) / 5)
    return get_texture(texture_names.fuel_bar[n])
}

export function get_rock_texture(_d: number) {
    return get_texture(texture_names.rocks[1])
}
