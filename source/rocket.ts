import { keys_down, keys_toggle, USE_TEXTURES } from "./index.ts";
import { get_animated_texture, get_fuel_texture, get_texture, texture_names } from "./resources.ts";

let stat_delta_t = 0

export const params = {
    mass: 2 * 1000,
    thrust: 30 * 1000,
    rot_speed: 2,
    fuel_consumption: 1 / 10,
    gravity: 9.81
}

export class Rocket {
    x = 50
    y = 70
    vx = 0
    vy = 0
    ax = 0
    ay = 0
    r = 2
    rot = 0
    fuel = 1

    constructor() {
    }

    tick(delta_t: number) {
        stat_delta_t = delta_t
        this.ax = 0
        this.ay = 0;

        let a_rot = 0, f_thrust = 0;
        if (keys_down.has("KeyW") || keys_down.has("ArrowUp")) f_thrust += params.thrust
        if (keys_down.has("KeyA") || keys_down.has("ArrowLeft")) a_rot += 1
        if (keys_down.has("KeyD") || keys_down.has("ArrowRight")) a_rot -= 1
        if (this.fuel <= 0) a_rot = 0, f_thrust = 0

        const m = params.mass
        const a_thrust = f_thrust / m;

        this.ax += -Math.sin(this.rot) * a_thrust
        this.ay += Math.cos(this.rot) * a_thrust

        this.ay -= params.gravity

        this.rot += delta_t * a_rot * params.rot_speed
        this.vx += this.ax * delta_t
        this.vy += this.ay * delta_t
        this.x += this.vx * delta_t
        this.y += this.vy * delta_t
        if (f_thrust != 0) this.fuel -= delta_t * params.fuel_consumption
    }

    draw(c: CanvasRenderingContext2D) {
        if (USE_TEXTURES) {
            c.save()
            c.translate(this.x, this.y)
            c.rotate(this.rot + Math.PI)
            const s = 1.2
            c.drawImage(get_texture(texture_names.rocket), -this.r * s, -this.r * s, this.r * 2 * s, this.r * 2 * s)
        } else {
            c.fillStyle = "white"
            c.beginPath()
            c.arc(this.x, this.y, this.r, 0 + this.rot, Math.PI * 2 + this.rot)
            c.closePath()
            c.fill()
        }

        if ((keys_down.has("KeyW") || keys_down.has("ArrowUp")) && this.fuel > 0) {
            if (USE_TEXTURES) {
                const s = this.r * 0.2;
                c.translate(this.r * 0.5, this.r * 1)
                c.drawImage(get_animated_texture(texture_names.flame, 10), -s, -s, s * 2, s * 2)
                c.translate(-this.r * 1.1, 0)
                c.drawImage(get_animated_texture(texture_names.flame, 10), -s, -s, s * 2, s * 2)
            } else {
                c.fillStyle = "red"
                c.beginPath()
                c.arc(this.x, this.y, this.r, Math.PI * 1.25 + this.rot, Math.PI * 1.25 + this.rot)
                c.arc(this.x, this.y, this.r * 2, Math.PI * 1.5 + this.rot, Math.PI * 1.5 + this.rot)
                c.arc(this.x, this.y, this.r, Math.PI * 1.75 + this.rot, Math.PI * 1.75 + this.rot)
                c.closePath()
                c.fill()
            }

        }

        c.restore()

        // c.fillStyle = "orange"
        // c.beginPath()
        // c.arc(this.x, this.y, this.r, Math.PI * 1.25 + this.rot, Math.PI * 1.75 + this.rot)
        // c.lineTo(this.x, this.y)
        // c.closePath()
        // c.fill()
    }

    drawHUD(c: CanvasRenderingContext2D) {
        const bar_w = 300;
        const bar_h = 100
        if (USE_TEXTURES) {
            c.drawImage(get_fuel_texture(this.fuel), 0, 0, bar_w, bar_h)

        } else {
            c.fillStyle = "green"
            c.fillRect(0, 0, bar_w * this.fuel, bar_h)
            c.strokeStyle = "white"
            c.strokeRect(0, 0, bar_w, bar_h)
            c.fillStyle = "white"
            c.font = "30px sans-serif"
            c.fillText(`Fuel: ${(this.fuel * 100).toFixed(0)}%`, 10, 35)
        }

        if (keys_toggle.has("KeyP")) {
            c.font = "20px sans-serif"
            c.fillStyle = "white"
            let down = 80
            const dstat = (s: string) => {
                c.fillText(s, 20, down)
                down += 20
            }
            dstat(`Δt: ${stat_delta_t.toFixed(3)}s`)
            dstat(`x: ${this.x.toFixed(2)}m`)
            dstat(`y: ${this.y.toFixed(2)}m`)
            dstat(`vx: ${this.vx.toFixed(2)}m/s`)
            dstat(`vy: ${this.vy.toFixed(2)}m/s`)
            dstat(`ax: ${this.ax.toFixed(2)}m/s²`)
            dstat(`ay: ${this.ay.toFixed(2)}m/s²`)
            dstat(`φ: ${this.rot.toFixed(2)}`)
            dstat(`m: ${params.mass.toFixed(1)}kg`)
            dstat(`r: ${this.r.toFixed(2)}m`)
            dstat(`g: ${params.gravity.toFixed(2)}m/s²`)
            dstat(`fthrust: ${params.thrust.toFixed(1)}N`)
        }
    }
}