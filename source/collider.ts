import { set_game_state, USE_TEXTURES } from "./index.ts";
import { get_rock_texture } from "./resources.ts";
import { Rocket } from "./rocket.ts";

export class Collider {
    texture_rot = Math.random() > 0.5 ? 0 : Math.PI

    constructor(
        public x: number,
        public y: number,
        public r: number
    ) { }

    draw(c: CanvasRenderingContext2D) {
        if (USE_TEXTURES) {
            c.save()
            c.translate(this.x, this.y)
            c.rotate(this.texture_rot)
            c.scale(1.05, 1.05)
            c.drawImage(get_rock_texture(this.r), -this.r, -this.r, this.r * 2, this.r * 2)
            c.restore()
        } else {
            c.fillStyle = "grey"
            c.beginPath()
            c.arc(this.x, this.y, this.r, 0, Math.PI * 2)
            c.closePath()
            c.fill()
        }
    }

    tick(r: Rocket) {
        const dx = r.x - this.x
        const dy = r.y - this.y
        const d = Math.sqrt(dx * dx + dy * dy)
        if (d < this.r + r.r) {
            const phi = Math.atan2(dx, dy)
            const safe = Math.abs(r.vx) < 3
                && Math.abs(r.vy) < 4
                && Math.abs(r.rot) < 0.2
                && Math.abs(phi) < 0.4
            console.log(JSON.stringify(r));


            set_game_state(safe ? "win" : "lose")
        }
    }
}