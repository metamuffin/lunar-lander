# Lunar lander

A recreation of the old lunar lander, with accurate™ physics

A hosted version of this can be found on [metamuffin.org](https://s.metamuffin.org/games/lunar-lander)
